const express = require('express');
const mongoose = require('mongoose');

const app = express();

const PORT = 3001;

// MongoDB
mongoose.connect(
  'mongodb+srv://admin123:admin123@project0.csykcns.mongodb.net/s35?retryWrites=true&w=majority',
  {
    useNewUrlParser: true, // compatibility
    useUnifiedTopology: true, // compatibility
  }
);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => console.log('Connected to database'));

// Schemas
const taskSchema = new mongoose.Schema({
  name: String,
  status: {
    type: String,
    default: 'pending',
  },
});

// Models
const Task = mongoose.model('Task', taskSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Create a new task
/*

  1. Check for duplciate tasks. T: return an error. F: add to db
  2. Task data will be in request body
  3. Create new Task with 'name' field
  4. The status is optional, default is 'pending'

*/
// Routes
app.post('/tasks', (req, res) => {
  Task.findOne({ name: req.body.name }, (err, result) => {
    if (result != null && result.name == req.body.name) {
      return res.send('Duplicate task found');
    } else {
      let newTask = new Task({
        name: req.body.name
      });
      newTask.save((saveErr, savedTask) => {
        if (saveErr) {
          return console.log(saveErr);
        } else {
          return res.status(201).send('New task created');
        }
      })
    }
  })
});

// Get all
/*

  1. Retrieve all tasks

*/
app.get('/tasks', (req, res) =>{
  Task.find({}, (err, result) => {
    if (err)
      return console.log(err);
    else
      return res.status(200).json({
        data: result
      })
  })
});

// == ACTIVITY ==
// Schema
const userSchema = new mongoose.Schema({
  username: String,
  password: String
});

// Model
const User = mongoose.model('User', userSchema);

// Routes
app.get('/users', (req, res) => {
  User.find({}, (err, result) => {
    if (err)
      console.log(err);
    else
      return res.status(200).json({
        userCount: result.length,
        data: result
      })
  });
});

app.post('/signup', (req, res) => {
  const newUsername = req.body.username;
  const newPassword = req.body.password;

  // gate
  if (!newUsername || !newPassword)
    return res.send('Please specify a username and password');

  const createNewUser = (username, password) => {
    const newUser = new User({
      username: username,
      password: password
    });

    newUser.save((saveErr, savedUser) => {
      if (saveErr)
        return console.log(saveErr);
      else
        return res.status(201).send('New user created')
    });
  };

  User.findOne({ username: newUsername }, (err, result) => {
    if (result != null && result.username == newUsername)
      return res.send(`Username ${newUsername} is already taken`)
    else
      createNewUser(newUsername, newPassword);
  });
});

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
